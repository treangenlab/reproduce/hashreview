###  HASH REVIEW

This repository contains scripts and results towards estimations reported in Table 2. in the paper. The time and memory requirements of the corresponding tools were
measured using `/usr/bin/time -v`.


#### Data:
##### Containment:[ 99 Sequencing Experiments](https://rice.box.com/s/ku43iszk0010cfoc55d6zp3ya8wpo26z) 
##### Classification and Profiling:[ Shakya et al. Metagenomic Community](https://rice.box.com/s/h3d44dho4klzlhqrdwtbrjvzn6u284zr)
##### Resemblance:[ 1028 Ecoli genomes](https://rice.box.com/s/j3mukoenr1hxj88h6sjcl5hkuk9gm2mg)
##### ProbeDesign:[ Coronovirus, West Nile, Zika, Yellow Fever, Ebola](https://rice.box.com/s/iuszmf2w9q4tv56ul4oqky3jxs6fpr0i)

Note: For [Kraken2](https://ccb.jhu.edu/software/kraken2/index.shtml?t=manual) and [KrakenUniq(KrakenDB)](https://github.com/fbreitwieser/krakenuniq) the index time and memory are based on the standard database reported by the authors on the tool webpage.

The table below indicates the actual quantitative estimates calculated towards Table 2 in the paper.Within reasonable limits given a category of tools, the stars (1-5) correspond to time (Days, Hours, Minutes, Seconds, and, Milliseconds) and Memory (>64 Gigabytes (server),>16 Gigabytes (workstation), > 1 Gigabyte, > 16 Megabytes and, < 16 Megabytes.  
The memory refers to maximum RAM used during index construction/query steps.`


| Category | Tool |	FileType |	WallClockTime (H:MM:S) |	MaxMem (KB) |
|---------|----|-----------|---------------|--------|
| containment | bloomtree |	index_tree_time (total) |	47:58.78 | 18733312 (Not including Jellyfish: 13214780) |
| containment | bloomtree |	search_query_time |	50:44.25  |  26790388 |
| containment | splitsbt	| index_tree_time (total) |	3:40:33.96   | 18733312 (Not including Jellyfish: 5117832) |
| containment | splitsbt	| search_query_time	| 4:55.61 | 2704848 |
| containment | BIGSI (ABF) |   index_time |  8:00.00 | 2320000 |
| containment | BIGSI(ABF) |  query_time |  2:00.00 | 2320000 |
| containment |RAMBO |  index_time |  7:30.00 | 3500000 |
| containment | RAMBO | query_time |  0:15.00 | 3500000 |
| containment | Mash_v2.2	| build_sketch_time |	1:54.29 | 10312 |
| containment | Mash_v2.2  | search_query_time |	0:01.72 | 117788 |
| probedesign | insense | insense_probe_time | 0.20.00 | 60000000 |
| probedesign | catch |	catch_probe_time |	17:24:42 | 19385064 |
| profiling | metapalette |	save_all_out |	1:56:15 | 77132056 |
| resemblance | mash | ecoli_sketch_time | 0:09.39 | 443876 |
| resemblance | mash | dist_time | 0:13.00 |	455380 |
| resemblance | kWIP | kwip_sketch_time |	2:00:16 | 1002504 |
| resemblance | kWIP | kwip_dist_time | 19:01:49 | 66422248 |
| resemblance | bindash |	sketch_time | 0:10.02 |	15824 |
| resemblance | bindash |	query_time | 0:00.30 |	166888 |
| resemblance | finch	 | finch_sketch_time |	0:49.32 | 19242228 |
| resemblance | finch |	finch_dist_time |	1:34.46 |	357292 |
| resemblance | dashing |	sketch_time |	0:04.80 |	175768 |
| resemblance | dashing |	dist_time |	0:00.64 | 6808 |
| resemblance | hulk | hulk_sketch_time |   23:42.63  |  1876 |
| resemblance | hulk | hulk_dist_time |  0:01.78 | 14876 |
| resemblance | sourmash | sourmash_sketch_time |	6:53.12 |	71968 |
| resemblance | sourmash | sourmash_distance_time | 0:00.29 |	54788 |
| classification | biobloom |	opal_data_index_time |	0:14.63 |	200712 |
| classification | biobloom |	opal_data_query_time |	3:09.41 |	60232 | 
| classification | krakenuniq | 	query_time |	12:19.49 |	175246392 |
| classification | opal	| opal_train_time |	20:28.71 |	67122408 |
| classification | opal	| opal_predict_time |	8:57:55 |	8404352 |
| classification | ganon |	ganon_build |	27:30.76 |	70847672 |
| classification | ganon	| shakya_all_time |	32:22.26 |	71084100 |
| classification | kraken2 |	kraken_query_time |	2:10.17 |	8811144 (run with miniKrakenV2 but original DB is 29 GB)|

[Timedoc.txt](https://gitlab.com/treangenlab/hashreview/-/blob/master/timedoc.txt) contains a more finegrained step wise index construction time and mem for sbt and ssbt

Note: Though insense and catch are both being grouped under probe design, they serve different purpose - Insense 
effectively ranks the D sensors in the bank to choose from and then  just grabs the top M of them. Speed and RAM were invariant of M . ince Insense picks nonspecific DNA sensors based on a set of 
characterized affinities between sensors and genomes, they'd probably have to be empirically characterized. D for the datast would therefore probably be < 1000 practically speaking.
