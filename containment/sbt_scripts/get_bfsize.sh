#!/bin/bash

# Directory storing fasta.gz files to be counted
fastaList="$1"
kmer=20
jfsize="200M"
threads=10

#tempfile="mer_counts.jf"

#Set up jellyfish generators file
touch generators

count=1
for file in $(ls $fastaList/*) ; do
	  if [ $count -eq 50 ]; then
        break
      fi
    cat $file >>  generators
    echo -ne "$count files processed \r"
    count=$((count+1))
    #echo  gunzip -c  $file >> generators
done 

echo "Begin counting"
/home/software/Jellyfish/bin/jellyfish count -m $kmer -s $jfsize -t $threads -C  generators #-G 1 #-g generators -G 1

# Don't need this anymore
#rm -rf generators 

#Output counts for 1, 2, 3+ occurence kmers
#echo "Unique canonical kmer counts by number of occurences:" 
#jellyfish histo mer_counts.jf -h 2

