'''
AUTHOR: ADVAIT BALAJI
INSTITUTION: TREANGEN LAB, RICE UNIVERSITY
DATE: MARCH 10th 2020
'''
import sys
import glob
import os
'''
INPUT: 
    COMMAND LINE ARG1 -> CATEGORY
    COMMAND LINE ARG2 -> TOOL
OUTPUT:
    WALL CLOCK AND MAXMEM APPENDED TO DOC.TXT FOR EACH FILE IN TOOL
'''
with open("doc.txt","a+") as of:
    #of.write("Category/Tool\tFileType\tWallClockTime\tMaxMem\n")
    files = glob.glob(os.path.join(sys.argv[1],sys.argv[2])+"/*.txt")
    for f in files:
        with open(f,"r") as rf:
            info = [os.path.join(sys.argv[1],sys.argv[2]), f.split("/")[-1].split(".txt")[0]]
            for line in rf:
                #if "User time (seconds):" in line:
                #   info.append(line.split(": ")[1])
                if "Elapsed (wall clock) time" in line:
                    info.append(line.split(": ")[1].strip("\n"))
                if "Maximum resident set size" in line:
                    info.append(line.split(": ")[1])
                    break;
        of.write("\t".join(info)+"\n")
                


